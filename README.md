# Firebase

Esta aplicación tiene como objetivo ejemplificar cada parte de firebase integrada por medio de expo y [rnfirebase](https://rnfirebase.io/database/usage)

Esta rama solo contiene la integración con [@react-native-firebase/app](https://www.npmjs.com/package/@react-native-firebase/app)

Debe tener encuenta que necesita el archivo de configuración para el sistema que quiera probar la app. [ios|android]


## 0 - Create app

```bash
expo init <app_name>
```

Steps to use expo with firebase native library

## 1 - Firebase
Configure the firebase android/ios application

> https://console.firebase.google.com


## 2 - Install dev-client & dependencies
```bash
$ expo install expo-dev-client
$ expo install @react-native-firebase/app
$ npm i @react-native-firebase/firestore
```

## 3 - Generate platform directories

```bash
$ expo run:android | expo run:ios
```

## 4 - Configuration files

Modify the following files:  

<details>
  <summary>🤖 <b>Android</b> </summary>

  > **/android/app/src/main/AndroidManifest.xml**  
  *Verify Package name*
  ```xml
  <manifest xmlns:android="http://schemas.android.com/apk/res/android" package="whiteapp.com">

  ```

  > **/android/build.gradle**  
  *Add google services*
  ```gradle
  buildscript {
    dependencies {
      // ... other dependencies
      classpath 'com.google.gms:google-services:4.3.10'
      // Add me --- /\
    }
  }
  ```

  > **/android/app/build.gradle**  
  *Execute the plugin*
  ```gradle
  apply plugin: 'com.android.application'
  apply plugin: 'com.google.gms.google-services' // <- Add this line
  ```

  > **/app.json**  
  *Add firebase app plugin*
  ```json
  expo {
    ...
    "plugins": [
        "@react-native-firebase/app"
      ]
  }
  ```


  > Place **/android/app/google-services.json**  
  Download it from Firebase

</details>


## 5 - RUN

Run the project so the native dependency it's included.

```bash
$ expo run:android | expo run:ios
```

References:
- https://docs.expo.dev/development/introduction/
- https://docs.expo.dev/development/getting-started/
- https://rnfirebase.io 

